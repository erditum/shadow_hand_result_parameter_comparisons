const mujoco_shadow_hand_touch_sensor = document.querySelector(".mujoco_shadow_hand_touch_sensor_p_tag");

mujoco_touch_sensor_value = {
    "robot0:TS_palm_fr": 0.207,
    "robot0:TS_mfproximal_front_right_top": 0.1222,
    "robot0:TS_rfproximal_front_right_top": 0.16529681149250683,
    "robot0:TS_palm_fl": 0.196969504000841
};

mujoco_shadow_hand_touch_sensor.innerHTML +=`<h3> <span style="color:red">Sensor Name </span> Sensor Value: <h3>`

for(var param in mujoco_touch_sensor_value) {
    mujoco_shadow_hand_touch_sensor.innerHTML +=`<p> <span style="color:red">${param}</span> ${mujoco_touch_sensor_value[param].toFixed(4)} </p>`
    console.log(param)
}   
    

mujoco_shadow_hand_touch_sensor.innerHTML +=`</table>`




const gazebo_shadow_hand_touch_sensor = document.querySelector(".gazebo_shadow_hand_touch_sensor_p_tag");

gazebo_shadow_hand_touch_sensor.innerHTML+=`<p> <span style="color:red">collision1_name: </span> "box::link::box_collision" </br>
<span style="color:red"> collision2_name:</span> "shadowhand_motor::rh_rfproximal::rh_rfproximal_collision" </br>
<p> <span style="color:blue">wrenches: </span> </br>
<p> <span style="color:green">      force: </span> </br>
        x: 0.05659802241119719 </br>
        y: 0.21087481435812194</br>
        z: 0.3844545486123881</br>
        <p> <span style="color:green">      torque: </span> </br>
        x: 0.0006199497872807605 </br>
        y: 0.000572485282574817 </br>
        z: -0.0004052771912893797 </br>
        <p> <span style="color:green">      force: </span> </br>
        x: 0.037743333269543385 </br>
        y: 0.06006047433861777 </br>
        z: 0.10323692417208569 </br>
        <p> <span style="color:green">      torque: </span> </br>
        x: -0.0009041767816975077 </br>
        y: -0.0002085295811786564 </br>
        z: 0.0004518831952766531 </br>
        <p> <span style="color:blue"> total_wrench : </span> </br> 
        <p> <span style="color:green">      force: </span> </br>
      x: 0.09434135568074058 </br>
      y: 0.2709352886967397 </br>
      z: 0.4876914727844738 </br>
      <p> <span style="color:green">      torque: </span> </br> 
      x: -0.00028422699441674724 </br>
      y: 0.0003639557013961606 </br>
      z: 4.660600398727344e-05 </br>
      <p> <span style="color:red "> contact_positions:  </span> </br>
      x: -0.03207096243484422 </br>
      y: 0.35369206849938034 </br>
      z: 0.47832668984448823 </br>
      </br>

      x: -0.030389042352155518 </br>
      y: 0.3439695405672733 </br>
      z: 0.4793798299733843 </br>
      <p> <span style="color:red ">  contact_normals:   </span> </br>
      x: 0.3513540829764005 </br>
      y: 0.16068999928330607 </br>
      z: 0.9223497343774453 </br>
      </br>
      x: 0.3513540829764005 </br>
      y: 0.16068999928330607 </br>
      z: 0.9223497343774453 </br>
      <p> <span style="color:red "> depths:  </span> </br>
      [0.004655075702419877, 0.004655075702419881]</p>`