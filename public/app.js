
function simulation_parameter_configurations(
    query_class_selector,
    mass,
    inertia_matrix_array,
    k_p,
    k_d,
    friction_1,
    friction_2,
    f_dir,
    physics_engine_parameter_dictionary

    )
{
// const first_video_parameters_element = document.querySelector(".first_video_parameters");
const first_video_parameters_element = document.querySelector(query_class_selector);

const mass_value = `m = ${mass}`;
const intertia_matrix = `\\begin{bmatrix}
I_{xx} & I_{xy} && I_{xz}  \\\\
I_{yx} & I_{yy} && I_{yz}  \\\\
I_{zx} & I_{zy} && I_{zz}
\\end{bmatrix}
=
\\begin{bmatrix}
${inertia_matrix_array[0][0]} & ${inertia_matrix_array[0][1]} && ${inertia_matrix_array[0][2]}  \\\\
${inertia_matrix_array[1][0]} & ${inertia_matrix_array[1][1]} && ${inertia_matrix_array[1][2]}  \\\\
${inertia_matrix_array[2][0]} & ${inertia_matrix_array[2][1]} && ${inertia_matrix_array[2][2]}
\\end{bmatrix}
`;

first_video_parameters_element.innerHTML += "<h3>Cube Physics Parameter</h3>";
first_video_parameters_element.innerHTML += "<h4>Intertia Matrix</h4>";

const intertia_matrix_katex_rendering = katex.renderToString(intertia_matrix, {
  throwOnError: false,
});

const mass_katex_rendering = katex.renderToString(mass_value, {
  throwOnError: false,
});
first_video_parameters_element.innerHTML += intertia_matrix_katex_rendering;
// first_video_parameters_element.innerHTML += "<h4>Mass</h4>";
first_video_parameters_element.innerHTML += mass_katex_rendering;
first_video_parameters_element.innerHTML += "<br>";
const stiffness = `\\text{Stiffness }k_{p} = ${k_p}`;
const stiffness_katex_rendering = katex.renderToString(stiffness, {
    throwOnError: false,
  });
first_video_parameters_element.innerHTML += stiffness_katex_rendering;
first_video_parameters_element.innerHTML += "<br>";
const damping = `\\text{Damping }k_{d} = ${k_d}`;
const damping_katex_rendering = katex.renderToString(damping, {
    throwOnError: false,
  });
first_video_parameters_element.innerHTML += damping_katex_rendering;

first_video_parameters_element.innerHTML += "<br>";
const friction1 = `\\text{Friction } \\mu_{1} = ${friction_1}`;
const friction1_katex_rendering = katex.renderToString(friction1, {
    throwOnError: false,
  });
first_video_parameters_element.innerHTML += friction1_katex_rendering;


first_video_parameters_element.innerHTML += "<br>";
const friction2 = `\\text{Friction } \\mu_{2} = ${friction_2}`;
const friction2_katex_rendering = katex.renderToString(friction2, {
    throwOnError: false,
  });
first_video_parameters_element.innerHTML += friction2_katex_rendering;



first_video_parameters_element.innerHTML += "<br>";
const friction_direction = `\\text{Friction direction vector } \\vec{f_{dir}} = ${f_dir}`;
const friction_direction_katex_rendering = katex.renderToString(friction_direction, {
    throwOnError: false,
  });
first_video_parameters_element.innerHTML += friction_direction_katex_rendering;


first_video_parameters_element.innerHTML += "<h4>Simulation Physics Parameter</h4>";
first_video_parameters_element.innerHTML += `<p>`

// first_video_parameters_element.innerHTML +=`<p>`
for(var param in physics_engine_parameter_dictionary) {
    first_video_parameters_element.innerHTML +=` <span class="red_text_bold"> ${param} = </span>${physics_engine_parameter_dictionary[param]} <br>`


}
first_video_parameters_element.innerHTML +=`</p>`

}




var physics_engine_config ={ 
"physics engine":"ODE",
"max_step_size":"0.1",
"real_time_update_rate":"0",
"max_contacts":"20",
"min_step_size": "0.0001",
"iters": "90",
"sor": "1.4",
"friction_model": "pyramid_model",
"cfm": "0",
"erp": "0.2",
"contact_max_correcting_vel": "100",
"contact_surface_layer": "0.001",

}


simulation_parameter_configurations(query_class_selector= ".first_video_parameters",
                  mass=0.2,
                  inertia_matrix_array = [[0,0,0],[1,1,1],[1,1,1]],
                  k_p="10^{25}",
                  k_d="10^{23}",
                  friction_1="100",
                  friction_2="100",
                  f_dir="[1,1,1]",
                  physics_engine_config)

var physics_engine_config ={ 
  "physics engine":"Bullet",
  "max_step_size":"0.1",
  "real_time_update_rate":"0",
  "max_contacts":"20",
  "min_step_size": "0.0001",
  "iters": "90",
  "sor": "1.4",
  "cfm": "0",
  "erp": "0.2",
  "contact_surface_layer": "0.001",
}
                    

simulation_parameter_configurations(query_class_selector= ".second_video_parameters",
mass=0.2,
inertia_matrix_array = [[0,0,0],[1,1,1],[1,1,1]],
k_p="10^{25}",
k_d="10^{23}",
friction_1="100",
friction_2="100",
f_dir="[1,1,1]",
physics_engine_config)


var physics_engine_config ={ 
  "physics engine":"Dart",
  "max_step_size":"0.1",
  "real_time_update_rate":"0",
  "max_contacts":"20",
}

simulation_parameter_configurations(query_class_selector= ".third_video_parameters",
mass=0.2,
inertia_matrix_array = [[0,0,0],[1,1,1],[1,1,1]],
k_p="10^{25}",
k_d="10^{23}",
friction_1="100",
friction_2="100",
f_dir="[1,1,1]",
physics_engine_config)


var physics_engine_config ={ 
  "physics engine":"Simbody",
  "max_step_size":"0.1",
  "real_time_update_rate":"0",
  "max_contacts":"20",
  "min_step_size": "0.0001",
  "accuracy":"0.001",
  "max_transient_velocity":"0.01",
  "stiffness":"1e08",
  "dissipation":"100",
  "plastic_coef_restitution":"0.5",
  "static_friction":"0.9",
  "dynamic_friction":"0.9",
  "viscous_friction":"0",
  "override_impact_capture_velocity":"0.001",
  "override_stiction_transition_velocity":"0.001"
}


simulation_parameter_configurations(query_class_selector= ".fourth_video_parameters",
mass=0.2,
inertia_matrix_array = [[0,0,0],[1,1,1],[1,1,1]],
k_p="10^{25}",
k_d="10^{23}",
friction_1="100",
friction_2="100",
f_dir="[1,1,1]",
physics_engine_config)
